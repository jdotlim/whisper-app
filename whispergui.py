from concurrent.futures import ProcessPoolExecutor
import os
import traceback

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
from tkinter import messagebox as mb

import whisper

MAX_PATH_DISPLAY_LEN = 15

class Transcriber:
    def __init__(self, size):
        self.model_size = size

    def transcribe(self, filepath):
        model = whisper.load_model(self.model_size)
        results = model.transcribe(filepath, verbose = False)
        return results


class WhisperAI:
    def __init__(self, root):
        self.inpath = ''
        self.outpath = ''
        self.model_size = tk.StringVar()
        self.model_size.set('tiny')

        self.root = root
        root.title('Whisper AI')
        root.columnconfigure(0, weight = 1)
        root.rowconfigure(0, weight = 1)

        mainframe = ttk.Frame(root)
        mainframe.grid(column = 0, row = 0, sticky = (tk.W, tk.E, tk.N, tk.S))
        # Sets how the columns and rows inside the frame should resize when the
        # frame is resized. The weight tells Tk the ratio of the sizes that the
        # rows / columns should get. So if two columns both have weight of 1,
        # they will resize equally.
        mainframe.columnconfigure([0, 1], weight = 1)
        mainframe.rowconfigure([0, 1, 2, 3], weight = 1)

        # padding is given clockwise starting from the west
        pathsframe = ttk.LabelFrame(
            mainframe, text = 'Input / Output', padding = (40, 10, 40, 10)
        )
        optionsframe = ttk.LabelFrame(
            mainframe, text = 'Speed / Accuracy', padding = (40, 10, 40, 10)
        )
        progressframe = ttk.Frame(mainframe, padding = (5, 20, 5, 20))

        # place the frames
        pathsframe.grid(
            column = 0, row = 0, rowspan = 3, sticky = (tk.NW, tk.SE)
        )
        pathsframe.columnconfigure(0, weight = 1)
        pathsframe.rowconfigure([0, 1, 2], weight = 1)
        optionsframe.grid(
            column = 1, row = 0, rowspan = 3, sticky = (tk.NW, tk.SE)
        )
        optionsframe.columnconfigure(0, weight = 1)
        optionsframe.rowconfigure([0, 1, 2], weight = 1)
        progressframe.grid(column = 0, row = 3, columnspan = 2)

        self.inpath_display = tk.StringVar()
        self.inpath_display.set('No input selected')
        self.outpath_display = tk.StringVar()
        self.outpath_display.set('No output selected')
        self.status_msg = tk.StringVar()
        self.status_msg.set('Ready')

        ttk.Button(
            pathsframe, text = 'Input', command = self.select_infile
        ).grid(
            column = 0, row = 0, sticky = (tk.W, tk.E), padx = 10, pady = 10
        )
        ttk.Label(
            pathsframe, textvariable = self.inpath_display
        ).grid(column = 0, row = 1, sticky = tk.W)

        ttk.Button(
            pathsframe, text = 'Output', command = self.select_outfile
        ).grid(
            column = 0, row = 2, sticky = (tk.W, tk.E), padx = 10, pady = 10
        )
        ttk.Label(
            pathsframe, textvariable = self.outpath_display
        ).grid(column = 0, row = 3, sticky = tk.W)

        ttk.Radiobutton(
            optionsframe, text = 'Fast/Low', variable = self.model_size,
            value = 'tiny'
        ).grid(column = 0, row = 0, sticky = (tk.W, tk.E))
        ttk.Radiobutton(
            optionsframe, text = 'Medium/Medium', variable = self.model_size,
            value = 'base'
        ).grid(column = 0, row = 1, sticky = (tk.W, tk.E))
        ttk.Radiobutton(
            optionsframe, text = 'Slow/High', variable = self.model_size,
            value = 'small'
        ).grid(column = 0, row = 2, sticky = (tk.W, tk.E))
        ttk.Button(
            optionsframe, text = 'Transcribe', command = self.run
        ).grid(column = 0, row = 3, sticky = (tk.W, tk.E))

        ttk.Label(progressframe, textvariable = self.status_msg).grid(
            column = 0, row = 0, columnspan = 2, sticky = (tk.W, tk.E)
        )


    def select_infile(self):
        filetypes = (
            ('mp3', '*.mp3'),
            ('mp4', '*.mp4'),
            ('All files', '*.*')
        )

        filename = fd.askopenfilename(
            title = 'Select input file',
            initialdir = os.path.expanduser('~'),
            filetypes = filetypes
        )
        
        if len(filename.strip()) > 0:
            self.inpath = filename
            self.inpath_display.set(self.format_path_for_display(filename))
        else:
            self.inpath = ''
            self.inpath_display.set('No input selected')


    def select_outfile(self):
        filetypes = (('txt', '*.txt'), ('All files', '*.*'))

        filename = fd.asksaveasfilename(
            title = 'Select output file',
            initialdir = os.path.expanduser('~'),
            filetypes = filetypes
        )

        if len(filename.strip()) > 0:
            self.outpath = filename
            self.outpath_display.set(self.format_path_for_display(filename))
        else:
            self.outpath = ''
            self.outpath_display.set('No output selected')


    def format_path_for_display(self, path):
        if len(path) <= MAX_PATH_DISPLAY_LEN:
            return path
        basename = os.path.basename(path)
        return f'{basename[0:(MAX_PATH_DISPLAY_LEN - 3)]}...'


    def run(self):
        if not self.inpath or not self.outpath:
            self.status_msg.set('Input and output must be selected')
            return

        model_size = self.model_size.get()
        self.status_msg.set(f'Transcribing with {model_size} model')
        inpath = self.inpath
        ts = Transcriber(model_size)
        try:
            with ProcessPoolExecutor(max_workers = 1) as executor:
                future = executor.submit(ts.transcribe, inpath)
                while future.running():
                    self.root.update_idletasks()
                result = future.result()
            with open(self.outpath, mode = 'w', encoding = 'utf-8') as f:
                segments = result['segments']
                for seg in segments:
                    if 'text' in seg and len(seg['text']) > 0:
                        f.write(f'{seg["text"]}\n')
            self.status_msg.set('Done')
        except Exception as e:
            traceback.print_exc()
            self.status_msg.set('Failed')

root = tk.Tk()
WhisperAI(root)
root.mainloop()
