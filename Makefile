.PHONY: run

run: whispergui.py
	poetry run python $<
